/*
 * Copyright (c) 2011, Willem Cazander
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this list of conditions and the
 *   following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 *   the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


//Short name   : PulseFire
//Short desc   : Automatic PulseFire Sequence Generator.
//First created: 26-Apr-2011
//First Author : Willem Cazander
//License-Type : BSD 2-Clause (see licence.txt)
//IO-Hardware  : see chip_avr.c and chip_avr_mega.c
//USB-Serial   : 115200b + "Newline" on enter/return
//Website      : http://www.nongnu.org/pulsefire/

#include "vars.h"
#include "serial.h"
#include "sys.h"
#include "vsc.h"
#include "ptc.h"
#include "ptt.h"
#include "stv.h"
#include "lcd.h"
#include "mal.h"
#include "adc.h"
#include "dic.h"
#include "freq.h"
#include "utils.h"
#include "chip.h"

// Main function
int main(void) {

	Chip_setup_serial(); // Setup serial first so we can debug.
	Chip_sei();          // Enable interrupts
	Serial_setup();      // Setup serial lib
	Vars_setup();        // Setup vars
	Chip_setup();        // Setup chips using vars

#if defined(SF_ENABLE_VSC0) || defined(SF_ENABLE_VSC1)
	Vsc_setup();         // Setup vars steps
#endif
#ifdef SF_ENABLE_PWM
	PWM_send_output(PULSE_DATA_OFF);
#endif
#ifdef SF_ENABLE_LCD
	Lcd_setup();         // needs interrupts in spi mode
#endif

	// FIXME: init-dep serial works after Chip_setup(); not before.
	Serial_println();
	Serial_printCharP(pmPromt);
	Serial_println();
	for(;;) {
		// High speed loop
		uint16_t loop_cnt = pf_data.sys_loop_cnt;
		if (loop_cnt > 0) {
			loop_cnt++;
			pf_data.sys_loop_cnt = loop_cnt;
		}
		Vars_loop();
		Serial_loop();
		Adc_loop();

		// Time divider function
		uint8_t run_loop = pf_data.sys_loop_run;
		if (run_loop == 0xFF) {
			continue;
		}
		pf_data.sys_loop_run = 0xFF; // zero is value so use max to flag runned
		uint8_t run_loop_one = run_loop & ONE;

		// Main 20 hz loop
		Chip_out_doc();
		Dic_loop();
#ifdef SF_ENABLE_VSC0
		Vsc_loop0();
#endif
#ifdef SF_ENABLE_VSC1
		Vsc_loop1();
#endif
#ifdef SF_ENABLE_PTC0
		Ptc_loop0();
#endif
#ifdef SF_ENABLE_PTC1
		Ptc_loop1();
#endif

		// Duel 10 hz loop
		if (run_loop_one == ZERO) {
#ifdef SF_ENABLE_MAL
			Mal_loop();
#endif
		}
		if (run_loop_one == ONE) {
#ifdef SF_ENABLE_PTT
			Ptt_loop();
#endif
		}

		// 5 Hz loop
#ifdef SF_ENABLE_LCD
		if ((run_loop & (ONE+ONE+ONE)) == ZERO) {
			Lcd_loop();
		}
#endif

		// Duel 1Hz loop
#ifdef SF_ENABLE_STV
		if (run_loop == 2) {
			Stv_loop();
		}
#endif
		if (run_loop == 12) {
			Chip_loop();
			Sys_loop();
		}
	}
}

// EOF

