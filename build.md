
# PulseFire Building

## Requirements

* java jdk 11 or higher.
* maven 3 or higher.
* make/avr-gcc (apt-get install make gcc-avr avr-libc avrdude)

## Create build artifacts

NOTE: run on java17 jvm so jlink cmd works.

cd project-root/;
mvn -Ppf-build clean package;

Which results in zip archieves in the build targets;
ls pulsefire-build/pulsefire-build-dist/target/*.zip


## Build manual chip code

cd project-root/;
cd pulsefire-chip/src/main/c/;
make clean atmega328p-1601;
or
make clean atmega328p-1601-isp;
or 
make clean all;

note: firmwares manual build do not show in application.

## Test Flash code manual

cd project-root/;
FIXME

## Change pom.xml versions

cd project-root/;
mvn versions:set -DnewVersion=2.3.4-SNAPSHOT

## Make release build

cd project-root/;
mvn -Ppf-build -B -Dusername=(scm_username) clean package release:clean release:prepare release:perform;
src/build/gnu-up.sh (scm_username) (version)

## Make site

cd project-root/;
mvn -Ppf-build-site clean package site:site
Optional add -DstagingDirectory=/tmp/pf-fullsite
And then manual upload.

## Check for dependency-updates

cd project-root/;
mvn versions:display-plugin-updates|grep ">"|uniq;
mvn versions:display-dependency-updates|grep ">"|uniq;

## Eclipse Setup

* Download Eclipse Luna EE (4.4) from; http://eclipse.org/downloads/
* Install via marketplace "AVR Eclipse Plugin" for basic c/c++ editors support.
* Clone remote git repro to local.
* Import project into workspace from git working dir.
* Import the other modules by Import -> Maven/ Excisting maven projects and the select the git working dir.

- Goto pulsefire-device-ui and open class PulseFireUI.
- Run PulseFireUI
 

