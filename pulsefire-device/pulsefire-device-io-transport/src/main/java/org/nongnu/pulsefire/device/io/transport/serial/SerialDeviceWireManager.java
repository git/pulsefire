/*
 * Copyright (c) 2011, Willem Cazander
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this list of conditions and the
 *   following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 *   the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.nongnu.pulsefire.device.io.transport.serial;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.TooManyListenersException;
import java.util.logging.Logger;

import jssc.SerialPort;
import jssc.SerialPortException;
import jssc.SerialPortList;

import org.nongnu.pulsefire.device.io.protocol.Command;
import org.nongnu.pulsefire.device.io.protocol.CommandName;
import org.nongnu.pulsefire.device.io.transport.AbstractDeviceWireManager;
import org.nongnu.pulsefire.device.io.transport.DeviceCommandRequest;
import org.nongnu.pulsefire.device.io.transport.DeviceWireManager;

/**
 * SerialDeviceWireManager to to connect to serial pulsefire device.
 * 
 * @author Willem Cazander
 * @see DeviceWireManager
 */
public class SerialDeviceWireManager extends AbstractDeviceWireManager {

	private Logger logger = null;
	private SerialDeviceWireThread serialThread = null;
	
	public SerialDeviceWireManager() {
		logger = Logger.getLogger(SerialDeviceWireManager.class.getName());
	}
	
	@Override
	public List<String> getDevicePorts() {
		List<String> result = new ArrayList<String>(10);
		try {
			result.addAll(Arrays.asList(SerialPortList.getPortNames())); // mm open ports to check.
		} catch (Throwable e) { // missing lib in jvm path...so no ports
			logger.warning("CommError: "+e.getMessage());
		}
		logger.info("Total ports found: "+result.size());
		return result;
	}
	
	@Override
	public boolean connect(String port) throws IOException, InterruptedException, TooManyListenersException, SerialPortException {
		if (isConnected()) {
			disconnect(false);
		}
		long startTime = System.currentTimeMillis();
		boolean done = false;
		connectPhase = "Opening port";connectProgress = 1;
		try {
			// Get the commport and config it.
			SerialPort serialPort = new SerialPort(port);
			serialPort.openPort();
			serialPort.setParams(SerialPort.BAUDRATE_115200, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
			serialPort.purgePort(SerialPort.PURGE_RXCLEAR + SerialPort.PURGE_TXCLEAR);
			
			// Create and start backend thread for serial communication.
			connectPhase = "Start IO thread";connectProgress = 2;
			serialThread = new SerialDeviceWireThread(this,serialPort);
			serialThread.start();
			
			// Wait on thread creation and starting
			while (serialThread.isRunning()==false) {
				Thread.sleep(200);	
			}
			
			// Let arduino boot max 10 secs for promt. (note sometimes we do not get prompt)
			connectPhase = "Arduino booting";connectProgress = 5;
			for (int i=0;i<100;i++) {
				Thread.sleep(100);
				if (serialThread==null) {
					return false;
				}
				if (serialThread.hasSeenPromt()) {
					break;
				}
			}
			
			// Get info_chip as fist in max 5 seconds reponse time
			connectPhase = "Check info_chip";connectProgress = 7;
			DeviceCommandRequest infoChip = requestCommand(new Command(CommandName.info_chip));
			for (int i=0;i<50;i++) {
				Thread.sleep(100);
				if (serialThread==null) {
					return false;
				}
				if (infoChip.getResponse()!=null) {
					logger.finer("Got first info_chip response.");
					break;
				}
				if (i==40 && infoChip.getResponse()==null) {
					infoChip = requestCommand(new Command(CommandName.info_chip)); // request again
				}
			}
			infoChip.waitForResponseChecked();
			
			// Let do rest of connect by abstract parent
			boolean result = doSafeConnect(infoChip);
			done = true;
			long stopTime = System.currentTimeMillis();
			logger.info("Succesfully connected in "+(stopTime-startTime)+" ms.");
			return result;
		} finally {
			if (done==false) {
				disconnect(true);
			}
		}
	}
	
	@Override
	public void disconnect(boolean error) {
		if (serialThread==null) {
			return; // already diconnected
		}
		if (error==false) {
			// close nicely so serial buffers do not get flooded.
			requestCommand(new Command(CommandName.req_tx_push,	"0")).waitForResponse(); // disable auto push
			requestCommand(new Command(CommandName.req_tx_hex,	"0")).waitForResponse(); // disable hex
		}
		
		// can be null with multiple clicks fired..
		if (serialThread!=null) { 
			serialThread.shutdown();
			serialThread = null;
		}
		super.disconnect(error);
	}
}
