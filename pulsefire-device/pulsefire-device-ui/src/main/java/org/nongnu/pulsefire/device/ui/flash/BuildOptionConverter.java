/*
 * Copyright (c) 2011, Willem Cazander
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this list of conditions and the
 *   following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 *   the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.nongnu.pulsefire.device.ui.flash;

import java.util.Collection;

import org.nongnu.pulsefire.lib.flash.info.FlashFirmware;
import org.nongnu.pulsefire.lib.flash.info.FlashFirmwareOption;
import org.nongnu.pulsefire.lib.flash.info.FlashFirmwareSet;
import org.nongnu.pulsefire.lib.flash.info.FlashFirmwareSetProgrammer;
import org.nongnu.pulsefire.lib.flash.info.FlashInfo;
import org.nongnu.pulsefire.lib.flash.info.FlashInfoText;
import org.nongnu.pulsefire.lib.flash.info.FlashInfoTextValue;

/**
 * BuildOptionConverter exports the build options to the new FlashInfo objects.
 * 
 * @author Willem Cazander
 */
public class BuildOptionConverter {
	
	private static FlashInfoText createFlashInfoText() {
		FlashInfoText fit = new FlashInfoText();
		fit.setTitle("PulseFire Chip Firmwares");
		//fit.setDescription("Lists all firmwares");
		//fit.setResourceBundleFile("some/where/in/zip");
		fit.addFirmwareFlag(new FlashInfoTextValue("SPI", "Serial Peripheral Interface"));
		fit.addFirmwareFlag(new FlashInfoTextValue("LCD", "Liques Crytal Display"));
		fit.addFirmwareFlag(new FlashInfoTextValue("PWM", "Pulse With Modulation"));
		fit.addFirmwareFlag(new FlashInfoTextValue("MAL", "Micro Assamlbly Language"));
		fit.addFirmwareFlag(new FlashInfoTextValue("STV", "Safety Treshhold Values"));
		fit.addFirmwareFlag(new FlashInfoTextValue("VFC", "Virtual Feedback Channels"));
		fit.addFirmwareFlag(new FlashInfoTextValue("CIP", "Chip Internal PWM"));
		fit.addFirmwareFlag(new FlashInfoTextValue("PTT", "Programmable Time Trigger"));
		fit.addFirmwareFlag(new FlashInfoTextValue("PTC0", "Programmable Time Controller 0"));
		fit.addFirmwareFlag(new FlashInfoTextValue("PTC1", "Programmable Time Controller 1"));
		fit.addFirmwareFlag(new FlashInfoTextValue("VSC0", "Variable Stepper Controller 0"));
		fit.addFirmwareFlag(new FlashInfoTextValue("VSC1", "Variable Stepper Controller 1"));
		
		fit.addFirmwareOption(new FlashInfoTextValue("F_CPU", "Speed"));
		return fit;
	}
	
	private static FlashFirmwareSet createFlashFirmwareSet(FlashInfo fi,String name,String mcu,String proto) {
		FlashFirmwareSet ffs = new FlashFirmwareSet(name);
		fi.addFlashFirmwareSet(ffs);
		
		FlashFirmwareSetProgrammer programmer = new FlashFirmwareSetProgrammer();
		programmer.setMcuType(mcu);
		programmer.setProtocol(proto);
		ffs.setProgrammer(programmer);
		
		return ffs;
	}
	
	public static FlashInfo convertBuildOptions(Collection<BuildOption> options) {
		FlashInfo fi = new FlashInfo();
		fi.setInfoText(createFlashInfoText());
		
		FlashFirmwareSet atmega168p = createFlashFirmwareSet(fi,"atmega168p","m168p","arduino");
		FlashFirmwareSet atmega328p = createFlashFirmwareSet(fi,"atmega328p","m328p","arduino");
		FlashFirmwareSet atmega1280 = createFlashFirmwareSet(fi,"atmega1280","m1280","arduino");
		FlashFirmwareSet atmega2560 = createFlashFirmwareSet(fi,"atmega2560","m2560","stk500v2");
		
		for (BuildOption bo:options) {
			FlashFirmware ff = new FlashFirmware();
			ff.setName(bo.name);
			ff.setDescription(bo.description);
			ff.setFirmwareFile("firmware/"+bo.name+"/pulsefire.hex");
			//if (bo.name.startsWith("atmega328p")) {
			//	ff.setSchematicFile("firmware/"+bo.name+"/pins.png");
			//}
			for (String flag:bo.flags) {
				if (flag.startsWith("-DSF_ENABLE_"))
				{
					flag = flag.substring(12);
				}
				ff.addFirmwareFlag(flag);
			}
			ff.addFirmwareOption(new FlashFirmwareOption("F_CPU", ""+bo.speed));
			
			if (bo.name.startsWith("atmega168p")) {
				atmega168p.addFirmware(ff);
			} else if (bo.name.startsWith("atmega328p")) {
				atmega328p.addFirmware(ff);
			} else if (bo.name.startsWith("atmega1280")) {
				atmega1280.addFirmware(ff);
			} else if (bo.name.startsWith("atmega2560")) {
				atmega2560.addFirmware(ff);
			}
		}
		return fi;
	}
}
