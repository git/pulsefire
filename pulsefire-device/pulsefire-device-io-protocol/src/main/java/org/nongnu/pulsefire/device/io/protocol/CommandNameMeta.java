/*
 * Copyright (c) 2011, Willem Cazander
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this list of conditions and the
 *   following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 *   the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.nongnu.pulsefire.device.io.protocol;

/**
 * CommandNameMetaType defines the different types of meta data the command names can have from the chip.
 * 
 * @author Willem Cazander
 */
public class CommandNameMeta {

	public static MetaValue<Integer> ID = new MetaValue<Integer>(MetaType.ID,-1);
	public static MetaValue<Integer> BIT_TYPE = new MetaValue<Integer>(MetaType.BIT_TYPE,0);
	public static MetaValue<Integer> BITS = new MetaValue<Integer>(MetaType.BITS,0);

	public static MetaValue<Boolean> MAP_INDEX = new MetaValue<Boolean>(MetaType.MAP_INDEX,false);
	public static MetaValue<Boolean> MAP_INDEX_TRIGGER = new MetaValue<Boolean>(MetaType.MAP_INDEX_TRIGGER,false);
	
	public static MetaValue<Long> MAX_VALUE = new MetaValue<Long>(MetaType.MAX_VALUE,65535l);
	public static MetaValue<Integer> MAX_INDEX_A = new MetaValue<Integer>(MetaType.MAX_INDEX_A,-1);
	public static MetaValue<Integer> MAX_INDEX_B = new MetaValue<Integer>(MetaType.MAX_INDEX_B,-1);
	
	public enum MetaType {
		ID,
		BIT_TYPE,
		BITS,
		
		MAP_INDEX,
		MAP_INDEX_TRIGGER,
		
		MAX_VALUE,
		MAX_INDEX_A,
		MAX_INDEX_B;
	}
	
	public static class MetaValue<T> {
		private final MetaType type;
		private final T defaultValue;
		public MetaValue(MetaType type,T defaultValue) {
			this.type=type;
			this.defaultValue=defaultValue;
		}
		public MetaType getType() {
			return type;
		}
		public T getDefaultValue() {
			return defaultValue;
		}
		public MetaValueHolder<T> toValue(CommandName cmdName,T value) {
			return new MetaValueHolder<T>(cmdName,type,value);
		}
	}
	
	public static class MetaValueHolder<T> {
		private final CommandName cmdName;
		private final MetaType type;
		private final T value;
		public MetaValueHolder(CommandName cmdName,MetaType type,T value) {
			this.cmdName=cmdName;
			this.type=type;
			this.value=value;
		}
		public CommandName getCommandName() {
			return cmdName;
		}
		public MetaType getType() {
			return type;
		}
		public T getValue() {
			return value;
		}
	}
}
