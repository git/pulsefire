module org.nongnu.pulsefire.device.ui {
	exports org.nongnu.pulsefire.device.ui.time;
	exports org.nongnu.pulsefire.device.ui.tabs;
	exports org.nongnu.pulsefire.device.ui.debug;
	exports org.nongnu.pulsefire.device.ui.components;
	exports org.nongnu.pulsefire.device.ui.flash;
	exports org.nongnu.pulsefire.device.ui.pull;
	exports org.nongnu.pulsefire.device.ui;
	
	opens org.nongnu.pulsefire.device.ui.resources;
	opens org.nongnu.pulsefire.device.ui.resources.colors;
	opens org.nongnu.pulsefire.device.ui.resources.images;
	
	requires bsaf;
	requires java.desktop;
	requires java.logging;
	requires org.nongnu.pulsefire.chip;
	requires org.nongnu.pulsefire.device.io.protocol;
	requires org.nongnu.pulsefire.device.io.transport;
	requires org.nongnu.pulsefire.lib.flash.avr;
}
