module bsaf {
	exports org.jdesktop.application;
	exports org.jdesktop.application.session;
	exports org.jdesktop.application.utils;
	
	opens org.jdesktop.application.resources;
	opens org.jdesktop.application.resources.icons;
	
	requires java.desktop;
	requires java.logging;
}
