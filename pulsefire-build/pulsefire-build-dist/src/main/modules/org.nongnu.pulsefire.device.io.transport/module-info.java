module org.nongnu.pulsefire.device.io.transport {
	exports org.nongnu.pulsefire.device.io.transport.serial;
	exports org.nongnu.pulsefire.device.io.transport;
	
	requires java.logging;
	requires jssc;
	requires org.nongnu.pulsefire.device.io.protocol;
}
