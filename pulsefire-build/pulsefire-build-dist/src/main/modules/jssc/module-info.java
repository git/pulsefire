module jssc {
	requires transitive org.scijava.nativelib;
	exports jssc;
	opens natives.linux_32;
	opens natives.linux_64;
	opens natives.linux_arm;
	opens natives.linux_arm64;
	opens natives.linux_ppc;
	opens natives.osx_64;
	opens natives.osx_arm64;
	opens natives.sunos_32;
	opens natives.sunos_64;
	opens natives.windows_32;
	opens natives.windows_64;
	opens natives.windows_arm64;
}
