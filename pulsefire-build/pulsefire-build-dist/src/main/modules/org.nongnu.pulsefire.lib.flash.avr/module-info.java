module org.nongnu.pulsefire.lib.flash.avr {
	exports org.nongnu.pulsefire.lib.flash.info;
	exports org.nongnu.pulsefire.lib.flash.avr;
	exports org.nongnu.pulsefire.lib.flash.ui.swing;
	exports org.nongnu.pulsefire.lib.flash.ui;
	exports org.nongnu.pulsefire.lib.flash.avr.avr;
	exports org.nongnu.pulsefire.lib.flash;
	
	requires java.desktop;
	requires java.logging;
	requires java.xml;
	requires java.xml.bind;
	requires jssc;
}
