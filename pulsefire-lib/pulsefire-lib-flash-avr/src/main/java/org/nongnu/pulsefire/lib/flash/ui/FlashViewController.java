/*
 * Copyright (c) 2011, Willem Cazander
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this list of conditions and the
 *   following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 *   the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.nongnu.pulsefire.lib.flash.ui;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.nongnu.pulsefire.lib.flash.FlashManager;
import org.nongnu.pulsefire.lib.flash.avr.FlashControllerConfig;
import org.nongnu.pulsefire.lib.flash.avr.FlashProgramController;
import org.nongnu.pulsefire.lib.flash.info.FlashFirmware;
import org.nongnu.pulsefire.lib.flash.info.FlashFirmwareOption;
import org.nongnu.pulsefire.lib.flash.info.FlashFirmwareSet;
import org.nongnu.pulsefire.lib.flash.info.FlashInfo;
import org.nongnu.pulsefire.lib.flash.info.FlashInfoTextValue;

public class FlashViewController {
	
	private final FlashControllerConfig flashConfig;
	private volatile FlashProgramController flashProgramController = null;
	private FlashFirmware selectedFirmware = null;
	private String filterFirmwareSet = null;
	private final Map<String,Boolean> filterFlags;
	private final Map<String,String> filterOptions;
	
	private final Map<String,List<String>> totalFlashOptions;
	private final List<String> totalFlashFlags;
	private final List<String> visibleFlashInfoFirmwareSetNames;
	private final Map<FlashFirmware,FlashFirmwareSet> visibleFlashInfoFirmwares;
	private final Map<Class<?>,List<FlashViewEventListener<?>>> eventListeners;
	private boolean visibleSchematicFile = false;
	private FlashInfo flashInfo;
	
	public FlashViewController() {
		flashConfig = new FlashControllerConfig();
		flashProgramController = null;
		totalFlashOptions = new HashMap<>();
		totalFlashFlags = new ArrayList<String>();
		visibleFlashInfoFirmwareSetNames = new ArrayList<String>();
		visibleFlashInfoFirmwares = new HashMap<>();
		
		filterFlags = new HashMap<>();
		filterOptions = new HashMap<>();
		eventListeners = new HashMap<>();
	}
	
	private List<FlashViewEventListener<?>> getEventListenersByType(Class<?> eventType) {
		List<FlashViewEventListener<?>> result = eventListeners.get(eventType);
		if (result == null) {
			result = new ArrayList<>();
			eventListeners.put(eventType, result);
		}
		return result;
	}
	
	public void addEventListener(Class<?> eventType,FlashViewEventListener<? extends FlashViewEvent> listener) {
		getEventListenersByType(eventType).add(listener);
	}
	
	public void removeEventListener(Class<?> eventType,FlashViewEventListener<? extends FlashViewEvent> listener) {
		getEventListenersByType(eventType).remove(listener);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected void fireEventListeners(Class<?> eventType,FlashViewEvent event) {
		List<FlashViewEventListener<?>> listeners = getEventListenersByType(eventType);
		for (int i=0;i<listeners.size();i++) {
			FlashViewEventListener listener = listeners.get(i);
			listener.doEvent(event);
		}
	}
	
	private void tmp_fillTreeDefaults(FlashInfo flashInfo) {
		for (FlashFirmwareSet ffs:flashInfo.getFlashFirmwareSets()) {
			if (ffs.getVisible() == null) {
				ffs.setVisible(true); // add root default
			}
			tmp_walkTreeFlashFirmwareSet(ffs,"");
		}
	}
	
	private void tmp_walkTreeFlashFirmwareSet(FlashFirmwareSet ffs,String prefix) {
		for (FlashFirmwareSet s:ffs.getFlashFirmwareSets()) {
			// Copy parent parameters to childs
			for (String parentFlag:ffs.getFirmwareFlags()) {
				s.addFirmwareFlag(parentFlag);
			}
			for (FlashFirmwareOption parentOption:ffs.getFirmwareOptions()) {
				s.addFirmwareOption(parentOption);
			}
			if (s.getVisible() == null) {
				s.setVisible(ffs.getVisible());
			}
			
			// then walk down
			tmp_walkTreeFlashFirmwareSet(s,prefix+" ");
		}
		
		// last fill firmwares
		for (FlashFirmware fs:ffs.getFirmwares()) {
			for (String parentFlag:ffs.getFirmwareFlags()) {
				fs.addFirmwareFlag(parentFlag);
			}
			for (FlashFirmwareOption parentOption:ffs.getFirmwareOptions()) {
				fs.addFirmwareOption(parentOption);
			}
			if (fs.getVisible() == null) {
				fs.setVisible(true);
			}
		}
	}
	
	public FlashInfo getFlashInfo() {
		return flashInfo;
	}
	
	public void setFlashInfo(FlashInfo flashInfo) {
		this.flashInfo = flashInfo;
		this.visibleFlashInfoFirmwareSetNames.clear();
		this.visibleFlashInfoFirmwares.clear();
		this.totalFlashFlags.clear();
		this.visibleSchematicFile = false;
		
		
		tmp_fillTreeDefaults(flashInfo);
		
		for (FlashFirmwareSet s:flashInfo.getFlashFirmwareSets()) {
			walkFlashFirmwareSet(s,"");
		}
		Collections.sort(totalFlashFlags);
		
		fireEventListeners(FlashViewModelLoad.class,new FlashViewModelLoad());
		fireEventListeners(FlashViewModelFilter.class,new FlashViewModelFilter());  // mm
	}
	
	private void walkFlashFirmwareSet(FlashFirmwareSet ffs,String prefix) {
		if (Boolean.TRUE.equals(ffs.getVisible())) {
			visibleFlashInfoFirmwareSetNames.add(ffs.getName());
		}
		for (FlashFirmware ff:ffs.getFirmwares()) {
			
			if (ff.getProgrammer() == null) {
				ff.setProgrammer(ffs.getProgrammer());
			}
			if (ff.getProgrammer() != null && ff.getProgrammer().getMcuSignature()==null && ff.getProgrammer().getMcuType()!=null) {
				ff.getProgrammer().setMcuSignature(FlashManager.getDeviceId(ff.getProgrammer().getMcuType()));
				
			}
			
			if (ff.getSchematicFile() == null && ffs.getSchematicFile()!=null) {
				ff.setSchematicFile(ffs.getSchematicFile());
			}
			if (Boolean.TRUE.equals(ff.getVisible())) {
				visibleFlashInfoFirmwares.put(ff,ffs);
				
				if (ff.getSchematicFile() != null) {
					visibleSchematicFile = true;
				}
			}
			for (String flag:ff.getFirmwareFlags()) {
				if (!totalFlashFlags.contains(flag)) {
					totalFlashFlags.add(flag);
				}
			}
			for (FlashFirmwareOption option:ff.getFirmwareOptions()) {
				List<String> values = totalFlashOptions.get(option.getName());
				if (values == null) {
					values = new ArrayList<String>();
					totalFlashOptions.put(option.getName(),values);
				}
				if (!values.contains(option.getValue())) {
					values.add(option.getValue());
				}
			}
		}
		for (FlashFirmwareSet s:ffs.getFlashFirmwareSets()) {
			walkFlashFirmwareSet(s,prefix+" ");
		}
	}
	
	/**
	 * @return the flashControllerConfig
	 */
	public FlashControllerConfig getFlashControllerConfig() {
		return flashConfig;
	}
	
	/**
	 * @return the flashProgramController
	 */
	public FlashProgramController getFlashProgramController() {
		return flashProgramController;
	}
	
	/**
	 * @return the visibleFInfoFirmwareSetNames
	 */
	public List<String> getVisibleFlashInfoFirmwareSetNames() {
		return visibleFlashInfoFirmwareSetNames;
	}
	
	/**
	 * @return the totalFlashFlags
	 */
	public List<String> getTotalFlashFlags() {
		return totalFlashFlags;
	}
	
	/**
	 * @return the totalFlashOptions
	 */
	public Map<String, List<String>> getTotalFlashOptions() {
		return totalFlashOptions;
	}
	
	/**
	 * @return the filteredFlashInfoFirmwares
	 */
	public List<FlashFirmware> getFilteredFlashInfoFirmwares() {
		List<FlashFirmware> result = new ArrayList<>(visibleFlashInfoFirmwares.size());
		for (FlashFirmware ff:visibleFlashInfoFirmwares.keySet()) {
			
			FlashFirmware ffs = visibleFlashInfoFirmwares.get(ff);
			if (getFilterFirmwareSet()!=null && !getFilterFirmwareSet().equals(ffs.getName())) {
				continue;
			}
			
			boolean skip = false;
			for (String filterFlag:filterFlags.keySet()) {
				Boolean value = filterFlags.get(filterFlag);
				if (value == null) {
					continue; // not supported
				}
				if (value) {
					if (!ff.getFirmwareFlags().contains(filterFlag)) {
						skip = true;
					}
				} else {
					if (ff.getFirmwareFlags().contains(filterFlag)) {
						skip = true;
					}
				}
				if (skip) {
					break;
				}
			}
			if (skip) {
				continue;
			}
			
			for (FlashFirmwareOption option:ff.getFirmwareOptions()) {
				for (String filterOption:filterOptions.keySet()) {
					String filterValue = filterOptions.get(filterOption);
					if (filterOption.equals(option.getName()) && filterValue!=null && !filterValue.equals(option.getValue())) {
						skip = true;
						break;
					}
				}
				if (skip) {
					break;
				}
			}
			if (skip) {
				continue;
			}
			
			
			result.add(ff);
		}
		return result;
	}
	
	/**
	 * @return the selectedFirmware
	 */
	public FlashFirmware getSelectedFirmware() {
		return selectedFirmware;
	}
	
	/**
	 * @param selectedFirmware the selectedFirmware to set
	 */
	public void setSelectedFirmware(FlashFirmware selectedFirmware) {
		this.selectedFirmware = selectedFirmware;
		fireEventListeners(FlashViewFirmwareSelectionEvent.class,new FlashViewFirmwareSelectionEvent());
	}
	
	/**
	 * @return the filterFirmwareSet
	 */
	public String getFilterFirmwareSet() {
		return filterFirmwareSet;
	}
	
	/**
	 * @param filterFirmwareSet the filterFirmwareSet to set
	 */
	public void setFilterFirmwareSet(String filterFirmwareSet) {
		this.filterFirmwareSet = filterFirmwareSet;
		fireEventListeners(FlashViewModelFilter.class,new FlashViewModelFilter());
	}
	
	public void putFilterFlag(String flag,boolean value) {
		filterFlags.put(flag,value);
		fireEventListeners(FlashViewModelFilter.class,new FlashViewModelFilter());
	}
	
	public void removeFilterFlag(String flag) {
		filterFlags.remove(flag);
		fireEventListeners(FlashViewModelFilter.class,new FlashViewModelFilter());
	}
	
	public void clearFilterFlags() {
		filterFlags.clear();
		fireEventListeners(FlashViewModelFilter.class,new FlashViewModelFilter());
	}
	
	public List<String> getFilterFlagsOrdered() {
		List<String> result = new ArrayList<>(filterFlags.size());
		result.addAll(filterFlags.keySet());
		Collections.sort(result);
		return result;
	}
	
	public void loadFilterOptions(String options) {
		for (String option: options.split(",")) {
			String[] filter = option.split("=");
			if (filter.length != 2) {
				continue;
			}
			putFilterOption(filter[0], filter[1]);
		}
	}
	
	public String saveFilterOptions() {
		StringBuilder buf = new StringBuilder();
		Iterator<String> keys = filterOptions.keySet().iterator();
		while (keys.hasNext()) {
			String key = keys.next();
			buf.append(key);
			buf.append("=");
			buf.append(filterOptions.get(key));
			if (keys.hasNext()) {
				buf.append(",");
			}
		}
		return buf.toString();
	}
	
	public void putFilterOption(String name,String value) {
		filterOptions.put(name,value);
		fireEventListeners(FlashViewModelFilter.class,new FlashViewModelFilter());
	}
	
	public void removeFilterOption(String name) {
		filterOptions.remove(name);
		fireEventListeners(FlashViewModelFilter.class,new FlashViewModelFilter());
	}
	
	public void clearFilterOptions() {
		filterOptions.clear();
		fireEventListeners(FlashViewModelFilter.class,new FlashViewModelFilter());
	}
	
	/**
	 * @return the visibleSchematicFile
	 */
	public boolean hasVisibleSchematicFile() {
		return visibleSchematicFile;
	}
	
	public String findFlagDescription(String flag) {
		if (getFlashInfo()==null) {
			return null;
		}
		if (getFlashInfo().getInfoText()==null) {
			return null;
		}
		for (FlashInfoTextValue txt: getFlashInfo().getInfoText().getFirmwareFlags()) {
			if (flag.equals(txt.getName())) {
				return txt.getValue();
			}
		}
		return null;
	}
	
	public String findOptionDescription(String option) {
		if (getFlashInfo()==null) {
			return null;
		}
		if (getFlashInfo().getInfoText()==null) {
			return null;
		}
		for (FlashInfoTextValue txt: getFlashInfo().getInfoText().getFirmwareFlags()) {
			if (option.equals(txt.getName())) {
				return txt.getValue();
			}
		}
		return null;
	}
}
