/*
 * Copyright (c) 2011, Willem Cazander
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this list of conditions and the
 *   following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 *   the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.nongnu.pulsefire.lib.flash.info;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 * FlashInfoText is optional resources to example option and flags into human readable form.
 * 
 * @author Willem Cazander
 */
public class FlashInfoText {
	
	private String title;
	private String description;
	private String resourceBundleFile;
	private final List<FlashInfoTextValue> firmwareFlags;
	private final List<FlashInfoTextValue> firmwareOptions;
	
	public FlashInfoText() {
		firmwareFlags = new ArrayList<FlashInfoTextValue>();
		firmwareOptions = new ArrayList<FlashInfoTextValue>();
	}
	
	/**
	 * @return the title
	 */
	@XmlAttribute(required=AbstractXMLMarshallerMeta.REQUIRED)
	public String getTitle() {
		return title;
	}
	
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	
	/**
	 * @return the description
	 */
	@XmlAttribute(required=AbstractXMLMarshallerMeta.REQUIRED)
	public String getDescription() {
		return description;
	}
	
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	/**
	 * @return the resourceBundleFile
	 */
	@XmlAttribute
	public String getResourceBundleFile() {
		return resourceBundleFile;
	}
	
	/**
	 * @param resourceBundleFile the resourceBundleFile to set
	 */
	public void setResourceBundleFile(String resourceBundleFile) {
		this.resourceBundleFile = resourceBundleFile;
	}
	
	/**
	 * @return the firmwareFlags
	 */
	@XmlElement(name=AbstractXMLMarshallerElement.FLASH_TEXT_FLAG)
	public List<FlashInfoTextValue> getFirmwareFlags() {
		return firmwareFlags;
	}
	
	/**
	 * @param firmwareFlag the firmwareFlag to add
	 */
	public void addFirmwareFlag(FlashInfoTextValue firmwareFlag) {
		firmwareFlags.add(firmwareFlag);
	}
	
	/**
	 * @return the firmwareOptions
	 */
	@XmlElement(name=AbstractXMLMarshallerElement.FLASH_TEXT_OPTION)
	public List<FlashInfoTextValue> getFirmwareOptions() {
		return firmwareOptions;
	}
	
	/**
	 * @param firmwareOption the firmwareOption to add
	 */
	public void addFirmwareOption(FlashInfoTextValue firmwareOption) {
		firmwareOptions.add(firmwareOption);
	}
}
