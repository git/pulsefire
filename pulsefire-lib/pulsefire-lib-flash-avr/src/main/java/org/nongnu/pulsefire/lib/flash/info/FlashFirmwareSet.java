/*
 * Copyright (c) 2011, Willem Cazander
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this list of conditions and the
 *   following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 *   the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.nongnu.pulsefire.lib.flash.info;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;

/**
 * FlashFirmwareSet is a container for FlashFirmwares.
 * 
 * @author Willem Cazander
 */
public class FlashFirmwareSet extends FlashFirmware {
	
	private final List<FlashFirmware> firmwares;
	private final List<FlashFirmwareSet> firmwareSets;
	
	public FlashFirmwareSet() {
		firmwares = new ArrayList<FlashFirmware>();
		firmwareSets = new ArrayList<FlashFirmwareSet>();
	}
	
	public FlashFirmwareSet(String name) {
		this();
		setName(name);
	}
	
	/**
	 * @return the firmwares
	 */
	@XmlElement(name=AbstractXMLMarshallerElement.FIRMWARE)
	public List<FlashFirmware> getFirmwares() {
		return firmwares;
	}
	
	/**
	 * @param firmware the firmware to add
	 */
	public void addFirmware(FlashFirmware firmware) {
		firmwares.add(firmware);
	}
	
	/**
	 * @return the firmwareSets
	 */
	@XmlElement(name=AbstractXMLMarshallerElement.FIRMWARE_SET)
	public List<FlashFirmwareSet> getFlashFirmwareSets() {
		return firmwareSets;
	}
	
	/**
	 * @param firmwareSet the firmwareSet to add
	 */
	public void addFlashFirmwareSet(FlashFirmwareSet firmwareSet) {
		firmwareSets.add(firmwareSet);
	}
}
