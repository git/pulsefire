/*
 * Copyright (c) 2011, Willem Cazander
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this list of conditions and the
 *   following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 *   the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.nongnu.pulsefire.lib.flash.ui.swing;

import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.SpringLayout;
import javax.swing.UIManager;
import javax.swing.table.TableCellRenderer;

import org.nongnu.pulsefire.lib.flash.info.FlashFirmware;
import org.nongnu.pulsefire.lib.flash.info.FlashFirmwareOption;
import org.nongnu.pulsefire.lib.flash.ui.FlashViewController;
import org.nongnu.pulsefire.lib.flash.ui.FlashViewEventListener;
import org.nongnu.pulsefire.lib.flash.ui.FlashViewModelLoad;

@SuppressWarnings("serial")
public class FlashFirmwareTableRenderer extends JPanel implements TableCellRenderer {

	private static final Logger LOG = Logger.getLogger(FlashFirmwareTableRenderer.class.getName());
	private JPanel contentPanel;
	private JPanel flagPanel;
	private JPanel optionPanel;
	private JLabel nameValue;
	private JLabel descriptionValue;
	private JLabel firmwareValue;
	private final Map<String,JLabel> flagLabels;
	private final Map<String,JLabel> optionLabels;
	private final FlashViewController flashViewController;
	
	public FlashFirmwareTableRenderer(FlashViewController flashViewController) {
		super(new GridLayout(1,1,0,0));
		
		flagLabels = new HashMap<String,JLabel>();
		optionLabels = new HashMap<String,JLabel>();
		contentPanel = createRowContent(); 
		
		setOpaque(true);
		setBorder(BorderFactory.createEmptyBorder(3, 6, 3, 6));
		setName("Table.cellRenderer");
		add(contentPanel);
		
		this.flashViewController=flashViewController;
		this.flashViewController.addEventListener(FlashViewModelLoad.class, new FlashViewEventListener<FlashViewModelLoad>() {
			@Override
			public void doEvent(FlashViewModelLoad event) {
				fireFirmwareStructureChanged(); // also redo columns
			}
		});
	}
	
	private JPanel createRowContent() {
		JPanel p = new JPanel(new SpringLayout());
		
		JLabel nameLabel = new JLabel("Name: ");
		nameValue = new JLabel();
		nameValue.setBorder(BorderFactory.createEmptyBorder(0, 3, 0, 0)); // align with flag and option pane
		
		p.add(nameLabel);
		p.add(nameValue);
		
		JLabel descriptionLabel = new JLabel("Description: ");
		descriptionValue = new JLabel();
		descriptionValue.setBorder(BorderFactory.createEmptyBorder(0, 3, 0, 0));
		
		p.add(descriptionLabel);
		p.add(descriptionValue);
		
		JLabel firmwareLabel = new JLabel("Firmware: ");
		firmwareValue = new JLabel();
		firmwareValue.setBorder(BorderFactory.createEmptyBorder(0, 3, 0, 0));
		
		p.add(firmwareLabel);
		p.add(firmwareValue);
		
		JLabel flagLabel = new JLabel("Flags: ");
		flagPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 3, 0));
		
		p.add(flagLabel);
		p.add(flagPanel);
		
		JLabel optionLabel = new JLabel("Options: ");
		optionPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 3, 0));
		
		p.add(optionLabel);
		p.add(optionPanel);
		
		p.setBorder(BorderFactory.createLineBorder(getForeground(), 1));
		SpringLayoutGrid.makeCompactGrid(p, 5, 2,3,3,1,1);
		
		return p;
	}
	
	private void fireFirmwareStructureChanged() {
		flagLabels.clear();
		flagPanel.removeAll();
		for (String flag:flashViewController.getTotalFlashFlags()) {
			JLabel flagLabel = new JLabel(flag);
			flagLabels.put(flag,flagLabel);
			flagPanel.add(flagLabel);
		}
		optionLabels.clear();
		optionPanel.removeAll();
		Map<String, List<String>> flashOptions = flashViewController.getTotalFlashOptions();
		for (String option:flashOptions.keySet()) {
			JLabel optionLabel = new JLabel(option);
			optionLabels.put(option,optionLabel);
			optionPanel.add(optionLabel);
		}
	}
	
	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		if (table == null) {
			return this;
		}
		if (!(value instanceof FlashFirmware)) {
			return this;
		}
		
		Color cf = isSelected?table.getSelectionForeground():table.getForeground();
		Color cb = isSelected?table.getSelectionBackground():table.getBackground();
		setForeground(cf);
		setBackground(cb);
		setFont(table.getFont());
		if (hasFocus && table.isCellEditable(row, column)) {
			setForeground( UIManager.getColor("Table.focusCellForeground") );
			setBackground( UIManager.getColor("Table.focusCellBackground") );
		}
		
		setValue((FlashFirmware)value);
		return this;
	}
	
	private void setValue(FlashFirmware ff) {
		nameValue.setText(ff.getName());
		descriptionValue.setText(ff.getDescription());
		firmwareValue.setText(ff.getFirmwareFile());
		
		for (JLabel c:flagLabels.values()) {
			c.setVisible(false);
		}
		for (String flag:ff.getFirmwareFlags()) {
			JLabel f = flagLabels.get(flag);
			if (f==null) {
				LOG.warning("no label for flag: "+flag);
				continue;
			}
			f.setVisible(true);
		}
		
		for (JLabel c:optionLabels.values()) {
			c.setVisible(false);
		}
		for (FlashFirmwareOption option:ff.getFirmwareOptions()) {
			JLabel f = optionLabels.get(option.getName());
			if (f==null) {
				LOG.warning("no label for option: "+option.getName());
				continue;
			}
			f.setVisible(true);
			f.setText(option.getName()+"="+option.getValue());
		}
	}
}
