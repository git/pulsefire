/*
 * Copyright (c) 2011, Willem Cazander
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this list of conditions and the
 *   following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 *   the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.nongnu.pulsefire.lib.flash.info;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * FlashInfo is root of firmware listing data. 
 * 
 * @author Willem Cazander
 */
@XmlRootElement(name=AbstractXMLMarshallerElement.FLASH_INFO)
public class FlashInfo {
	
	private String defaultFirmwareSet;
	private FlashInfoText infoText;
	private List<FlashFirmwareSet> firmwareSets;
	
	public FlashInfo() {
		firmwareSets = new ArrayList<FlashFirmwareSet>(20);
	}
	
	/**
	 * @return the defaultFirmwareSet
	 */
	@XmlAttribute
	public String getDefaultFirmwareSet() {
		return defaultFirmwareSet;
	}
	
	/**
	 * @param defaultFirmwareSet the defaultFirmwareSet to set
	 */
	public void setDefaultFirmwareSet(String defaultFirmwareSet) {
		this.defaultFirmwareSet = defaultFirmwareSet;
	}
	
	/**
	 * @return the infoText
	 */
	@XmlElement(name=AbstractXMLMarshallerElement.FLASH_TEXT)
	public FlashInfoText getInfoText() {
		return infoText;
	}
	
	/**
	 * @param infoText the infoText to set
	 */
	public void setInfoText(FlashInfoText infoText) {
		this.infoText = infoText;
	}
	
	/**
	 * @return the firmwareSets
	 */
	@XmlElement(name=AbstractXMLMarshallerElement.FIRMWARE_SET)
	public List<FlashFirmwareSet> getFlashFirmwareSets() {
		return firmwareSets;
	}
	
	/**
	 * @param firmwareSet the firmwareSet to add
	 */
	public void addFlashFirmwareSet(FlashFirmwareSet firmwareSet) {
		firmwareSets.add(firmwareSet);
	}
}
