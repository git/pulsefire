/*
 * Copyright (c) 2011, Willem Cazander
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this list of conditions and the
 *   following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 *   the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.nongnu.pulsefire.lib.flash.info;

import javax.xml.bind.annotation.XmlAttribute;

/**
 * FlashFirmwareSetProgrammer holds the programmer info for all firmwares in the firmwareset.
 * 
 * @author Willem Cazander
 */
public class FlashFirmwareSetProgrammer {
	
	private Integer mcuSignature;
	private String mcuType;
	private String protocol;
	private Boolean flashVerify;
	
	public FlashFirmwareSetProgrammer() {
		flashVerify = false;
	}
	
	/**
	 * @return the mcuSignature
	 */
	@XmlAttribute
	public Integer getMcuSignature() {
		return mcuSignature;
	}
	
	/**
	 * @param mcuSignature the mcuSignature to set
	 */
	public void setMcuSignature(Integer mcuSignature) {
		this.mcuSignature = mcuSignature;
	}
	
	/**
	 * @return the mcuType
	 */
	@XmlAttribute(required=true)
	public String getMcuType() {
		return mcuType;
	}
	
	/**
	 * @param mcuType the mcuType to set
	 */
	public void setMcuType(String mcuType) {
		this.mcuType = mcuType;
	}
	
	/**
	 * @return the protocol
	 */
	@XmlAttribute(required=true)
	public String getProtocol() {
		return protocol;
	}
	
	/**
	 * @param protocol the protocol to set
	 */
	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}
	
	/**
	 * @return the flashVerify
	 */
	@XmlAttribute
	public Boolean getFlashVerify() {
		return flashVerify;
	}
	
	/**
	 * @param flashVerify the flashVerify to set
	 */
	public void setFlashVerify(Boolean flashVerify) {
		this.flashVerify = flashVerify;
	}
}
