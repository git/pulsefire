/*
 * Copyright (c) 2011, Willem Cazander
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this list of conditions and the
 *   following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 *   the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.nongnu.pulsefire.lib.flash.ui.swing;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

import javax.swing.DefaultComboBoxModel;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;

import jssc.SerialPortList;

/**
 * SerialPortsComboBoxModel
 * 
 * @author Willem Cazander
 */
@SuppressWarnings("serial")
public class SerialPortsComboBoxModel extends DefaultComboBoxModel<String> {
	
	private final Logger logger = Logger.getLogger(SerialPortsComboBoxModel.class.getName());
	private final PopupMenuListener popupListener;
	
	public SerialPortsComboBoxModel() {
		super();
		refreshDevicePorts();
		popupListener = new ReloadSerialPortPopupMenuListener();
	}
	
	public PopupMenuListener getPopupMenuListener() {
		return popupListener;
	}
	
	private void refreshDevicePorts() {
		removeAllElements();
		List<String> ports = findNativeDevicePorts();
		for (String port:ports) {
			addElement(port);
		}
	}
	
	private List<String> findNativeDevicePorts() {
		List<String> result = new ArrayList<String>(10);
		try {
			// mm open ports to check
			result.addAll(Arrays.asList(SerialPortList.getPortNames()));
		} catch (Throwable e) { // missing lib in jvm path...so no ports
			logger.warning("CommError: " + e.getMessage());
		}
		logger.info("Total ports found: " + result.size());
		return result;
	}
	
	private class ReloadSerialPortPopupMenuListener implements PopupMenuListener {
		@Override
		public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
			refreshDevicePorts();
		}
		
		@Override
		public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
		}
		
		@Override
		public void popupMenuCanceled(PopupMenuEvent e) {
		}
	}
}
