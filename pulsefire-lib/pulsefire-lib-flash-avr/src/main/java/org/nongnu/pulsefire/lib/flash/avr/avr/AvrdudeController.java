/*
 * Copyright (c) 2011, Willem Cazander
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this list of conditions and the
 *   following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 *   the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.nongnu.pulsefire.lib.flash.avr.avr;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.nongnu.pulsefire.lib.flash.avr.AbstractFlashProgramController;
import org.nongnu.pulsefire.lib.flash.avr.FlashControllerConfig;
import org.nongnu.pulsefire.lib.flash.avr.FlashException;
import org.nongnu.pulsefire.lib.flash.avr.FlashHexReader;


/**
 * AvrdudeController flashes with avrdude cmd.
 * 
 * @author Willem Cazander
 */
public class AvrdudeController extends AbstractFlashProgramController {

	@Override
	public void flashTimedRun(FlashControllerConfig conf) throws FlashException, IOException {
		if (conf.getNativeFlashCmd()==null) {
			throw new FlashException("Can't flash with null nativeFlashCmd in config.");
		}
		if (conf.getNativeFlashCmd().isEmpty()) {
			throw new FlashException("Can't flash with empty nativeFlashCmd in config.");
		}
		File nativeCmd = new File(conf.getNativeFlashCmd()); 
		if (nativeCmd.canExecute()==false) {
			throw new FlashException("Can't execute the nativeFlashCmd in config.");
		}
		setProgress(1);

		List<String> cmd = new ArrayList<String>(15);
		cmd.add(nativeCmd.getAbsolutePath());
		
		// HACK loopup table :(
		String chipId = null;
		if (conf.getDeviceSignature()==0x1e940b) {
			chipId = "m168p";
		} else if (conf.getDeviceSignature()==0x1e950f) {
			chipId = "m328p";
		} else if (conf.getDeviceSignature()==0x1e9703) {
			chipId = "m1280";
		} else if (conf.getDeviceSignature()==0x1e9801) {
			chipId = "m2560";
		} else {
			throw new FlashException("unknown device id in conf: "+conf.getDeviceSignature());
		}
		cmd.add("-p");
		cmd.add(chipId);
		cmd.add("-P");
		cmd.add(conf.getPort());
		cmd.add("-c");
		cmd.add(conf.getPortProtocol());
		if (conf.isLogDebug()) {
			cmd.add("-v");
			cmd.add("-v");
		}
		if (conf.isFlashVerify()==false) {
			cmd.add("-V");
		}
		if (conf.getNativeFlashConfig()!=null && conf.getNativeFlashConfig().isEmpty()==false) {
			cmd.add("-C");
			cmd.add(conf.getNativeFlashConfig());
		}
		
		File tmp = File.createTempFile("pulsefire-", ".hex");
		tmp.deleteOnExit();
		FlashHexReader hex = new FlashHexReader();
		hex.writeHexData(conf.getFlashData(), new FileOutputStream(tmp));
		cmd.add("-U");
		
		String fullFilePath = tmp.getAbsolutePath();
		if (fullFilePath.contains(":")) {
			fullFilePath = "\""+fullFilePath+"\""; // make bug report avrdude this is needed on windows but not under linux !!!
		}
		cmd.add("flash:w:"+fullFilePath+":i"); // win32 avrdude needs the format !!
		
		StringBuilder buf = new StringBuilder(200);
		for (String c:cmd) {
			buf.append(c);
			buf.append(' ');
		}
		logMessage("Exec: "+buf.toString());
		
		setProgress(2);
		ProcessBuilder builder = new ProcessBuilder(cmd);
		builder.redirectErrorStream(true);
		Process process = builder.start();
		InputStream stdout = process.getInputStream ();
		BufferedReader reader = new BufferedReader(new InputStreamReader(stdout));
		
		char c = 0;
		int writeLine = -1;
		
		StringBuilder lineBuf = new StringBuilder();
		while ((c = (char) reader.read()) != 0xFFFF) { // note: cast loops -1
			if (c == '\n' || c=='\r') {
				if (lineBuf.length() == 0) {
					continue;
				}
				String line = lineBuf.toString();
				lineBuf = new StringBuilder();
				
				if (line.contains("device initialized")) {
					setProgress(2);
				}
				if (line.contains("signature")) {
					setProgress(10);
				}
				
				if (line.contains("writing flash")) {
					writeLine = 0;
				}
				if (line.contains("flash written")) {
					writeLine = -1;
				}
				logMessage(line);
			} else {
				lineBuf.append(c);
				if (writeLine >= 0 && c == '#') {
					writeLine++;
					if (writeLine > 5) {
						setProgress((writeLine-5)*2+10);
					}
				}
			}
		}
		setProgress(100);
		logMessage("Avrdude is done.");
	}
}
