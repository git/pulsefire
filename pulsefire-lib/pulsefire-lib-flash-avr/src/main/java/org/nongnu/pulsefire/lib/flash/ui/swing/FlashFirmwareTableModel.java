/*
 * Copyright (c) 2011, Willem Cazander
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this list of conditions and the
 *   following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 *   the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.nongnu.pulsefire.lib.flash.ui.swing;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import org.nongnu.pulsefire.lib.flash.info.FlashFirmware;
import org.nongnu.pulsefire.lib.flash.ui.FlashViewController;
import org.nongnu.pulsefire.lib.flash.ui.FlashViewEventListener;
import org.nongnu.pulsefire.lib.flash.ui.FlashViewModelFilter;

@SuppressWarnings("serial")
public class FlashFirmwareTableModel extends AbstractTableModel {

	private final List<FlashFirmware> filteredFirmwares;
	
	public FlashFirmwareTableModel(final FlashViewController flashViewController) {
		this.filteredFirmwares=new ArrayList<>();
		flashViewController.addEventListener(FlashViewModelFilter.class, new FlashViewEventListener<FlashViewModelFilter>() {
			@Override
			public void doEvent(FlashViewModelFilter event) {
				filteredFirmwares.clear();
				filteredFirmwares.addAll(flashViewController.getFilteredFlashInfoFirmwares());
				fireTableDataChanged();
			}
		});
	}
	
	@Override
	public int getColumnCount() {
		return 1;
	}
	
	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return FlashFirmware.class;
	}
	
	@Override
	public int getRowCount() {
		return filteredFirmwares.size();
	}
	
	@Override
	public Object getValueAt(int row,int col) {
		return filteredFirmwares.get(row);
	}
}
