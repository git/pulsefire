/*
 * Copyright (c) 2011, Willem Cazander
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this list of conditions and the
 *   following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 *   the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.nongnu.pulsefire.lib.flash.info;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.Objects;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.ValidationEvent;
import javax.xml.bind.ValidationEventHandler;

import org.xml.sax.SAXException;

/**
 * XMLMarshallerFlashInfo converts the FlashInfo to xml and back. 
 * 
 * @author Willem Cazander
 */
public class XMLMarshallerFlashInfo {
	
	private final JAXBContext flashInfoContext;
	//private final Schema flashInfoSchema;
	private final ValidationEventHandler validationEventHandler;
	
	public XMLMarshallerFlashInfo() throws JAXBException, SAXException {
		this(new ValidationEventHandler() {
			@Override
			public boolean handleEvent(ValidationEvent event) {
				System.out.println("\nEVENT");
				System.out.println("SEVERITY:  " + event.getSeverity());
				System.out.println("MESSAGE:  " + event.getMessage());
				System.out.println("LINKED EXCEPTION:  " + event.getLinkedException());
				System.out.println("LOCATOR");
				System.out.println("\tLINE NUMBER:  " + event.getLocator().getLineNumber());
				System.out.println("\tCOLUMN NUMBER:  " + event.getLocator().getColumnNumber());
				System.out.println("\tOFFSET:  " + event.getLocator().getOffset());
				System.out.println("\tOBJECT:  " + event.getLocator().getObject());
				System.out.println("\tNODE:  " + event.getLocator().getNode());
				System.out.println("\tURL:  " + event.getLocator().getURL());
				return true;
			}
			
		});
	}
	
	public XMLMarshallerFlashInfo(ValidationEventHandler validationEventHandler) throws JAXBException, SAXException {
		Objects.requireNonNull(validationEventHandler);
		this.validationEventHandler = validationEventHandler;
		
		flashInfoContext = JAXBContext.newInstance(FlashInfo.class);
		
		//SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		//flashInfoSchema = null;//sf.newSchema(new File("customer.xsd"));
	}
	
	public void marshal(FlashInfo rootNode, OutputStream output) throws JAXBException {
		Objects.requireNonNull(rootNode,"FlashInfo is null.");
		Objects.requireNonNull(output,"OutputStream is null.");
		
		Marshaller marshaller = flashInfoContext.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
		//marshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, "http://pulsefire.forwardfire.net/xml/ns/flash-info test.xsd");
		//marshaller.setSchema(flashInfoSchema);
		marshaller.setEventHandler(validationEventHandler);
		marshaller.marshal(rootNode, output);
	}
	
	public FlashInfo unmarshal(InputStream input) throws JAXBException {
		Objects.requireNonNull(input,"InputStream is null.");
		
		Unmarshaller unmarshaller = flashInfoContext.createUnmarshaller();
		//unmarshaller.setSchema(flashInfoSchema);
		unmarshaller.setEventHandler(validationEventHandler);
		FlashInfo result = (FlashInfo) unmarshaller.unmarshal(input);
		return result;
	}
}
