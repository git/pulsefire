/*
 * Copyright (c) 2011, Willem Cazander
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this list of conditions and the
 *   following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 *   the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.nongnu.pulsefire.lib.flash.ui.swing;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SpringLayout;
import javax.swing.SwingUtilities;
import javax.swing.ToolTipManager;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.nongnu.pulsefire.lib.flash.FlashManager;
import org.nongnu.pulsefire.lib.flash.avr.FlashControllerConfig;
import org.nongnu.pulsefire.lib.flash.avr.FlashEventListener;
import org.nongnu.pulsefire.lib.flash.avr.FlashHexReader;
import org.nongnu.pulsefire.lib.flash.avr.FlashProgramController;
import org.nongnu.pulsefire.lib.flash.info.FlashFirmware;
import org.nongnu.pulsefire.lib.flash.info.FlashFirmwareSet;
import org.nongnu.pulsefire.lib.flash.info.FlashFirmwareSetProgrammer;
import org.nongnu.pulsefire.lib.flash.info.FlashInfo;
import org.nongnu.pulsefire.lib.flash.ui.FlashViewController;
import org.nongnu.pulsefire.lib.flash.ui.FlashViewEventListener;
import org.nongnu.pulsefire.lib.flash.ui.FlashViewFirmwareSelectionEvent;
import org.nongnu.pulsefire.lib.flash.ui.FlashViewModelLoad;

public class FlashSwingUI {
	
	private final FlashViewController flashViewController;
	private final FlashSwingUIIntegration flashSwingUIIntegration;
	private final Map<String, ImageIcon> schemaMap = new HashMap<>();
	private Logger logger = Logger.getLogger(FlashSwingUI.class.getName());
	private boolean allowSelectionEvent = true;
	private JComboBox<String> firmwareSetBox = null;
	private JComboBox<String> optionsBox = null;
	private JList<String> filterFlags;
	private JComboBox<String> portsComboBox = null;
	private JComboBox<String> progComboBox = null;
	private JCheckBox progVerboseBox = null; 
	private JCheckBox progVerifyBox = null;
	private JProgressBar flashProgressBar = null;
	private JTextArea flashLog = null;
	private DateFormat flashLogTimeFormat = null;
	private JButton closeButton = null;
	private JButton flashButton = null;
	private JButton exportButton = null;
	private FlashFirmwareTableModel tableModel = null;
	private JTable table = null;
	private JPanel schemaPanel = null;
	private JLabel schemaLabel = null;
	private JLabel burnName = null;
	private JLabel burnDeviceId = null;
	private JFrame frame;
	
	public FlashSwingUI(FlashSwingUIIntegration flashSwingUIIntegration) {
		this.flashViewController = new FlashViewController();
		this.flashSwingUIIntegration = flashSwingUIIntegration;
		this.flashSwingUIIntegration.init(flashViewController);
	}
	
	public interface FlashSwingUIIntegration {
		JPanel createTitledBorderPanel(String title);
		void doWindowClose(FlashViewController controller, Window window);
		String getExportFilePrefix();
		
		InputStream loadSchemaImage(FlashFirmware firmware);
		InputStream loadHexFile(FlashFirmware firmware);
		
		void init(FlashViewController controller);
	}
	
	public JFrame openFrame(FlashInfo flashInfo, JFrame optionalParent) {
		frame = new JFrame();
		frame.setTitle("Flash Chip Firmware");
		frame.setMinimumSize(new Dimension(640,480));
		frame.setPreferredSize(new Dimension(999,666));
		frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we) {
				flashSwingUIIntegration.doWindowClose(flashViewController, frame);
			}
		});
		JPanel mainPanel = new JPanel();
		mainPanel.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
		mainPanel.setLayout(new BorderLayout());
		mainPanel.add(createPanelTop(),BorderLayout.WEST);
		mainPanel.add(createPanelCenter(),BorderLayout.CENTER);
		mainPanel.add(createPanelBottom(),BorderLayout.SOUTH);
		frame.getContentPane().add(mainPanel);
		frame.pack();
		if (optionalParent!=null) {
			frame.setLocationRelativeTo(optionalParent);
			frame.setIconImage(optionalParent.getIconImage());
		}
		frame.setVisible(true);
		
		flashViewController.addEventListener(FlashViewModelLoad.class, new FlashViewEventListener<FlashViewModelLoad>() {
			@Override
			public void doEvent(FlashViewModelLoad event) {
				updateGUI();
			}
		});
		flashViewController.addEventListener(FlashViewFirmwareSelectionEvent.class, new FlashViewEventListener<FlashViewFirmwareSelectionEvent>() {
			@Override
			public void doEvent(FlashViewFirmwareSelectionEvent event) {
				updateSelection();
			}
		});
		
		flashViewController.setFlashInfo(flashInfo);
		return frame;
	}
	
	private void updateGUI() {
		allowSelectionEvent = false;
		try {
			List<String> flagData = new ArrayList<>();
			for (String flag: flashViewController.getTotalFlashFlags()) {
				String flagTxt = flashViewController.findFlagDescription(flag);
				if (flagTxt == null) {
					flagData.add(flag);
				} else {
					flagData.add(flag+": "+flagTxt);
				}
			}
			filterFlags.setListData(flagData.toArray(new String[]{}));
			firmwareSetBox.removeAllItems();
			firmwareSetBox.addItem("ALL");
			for (String i:flashViewController.getVisibleFlashInfoFirmwareSetNames()) {
				firmwareSetBox.addItem(i);
			}
			if (flashViewController.getFilterFirmwareSet() != null) {
				firmwareSetBox.setSelectedItem(flashViewController.getFilterFirmwareSet());
			}
			optionsBox.removeAllItems();
			optionsBox.addItem("ALL");
			for (String i:flashViewController.getTotalFlashOptions().keySet()) {
				for (String value: flashViewController.getTotalFlashOptions().get(i)) {
					optionsBox.addItem(i+"="+value);
				}
			}
			String savedOptions = flashViewController.saveFilterOptions();
			if (!savedOptions.isEmpty()) {
				if (!savedOptions.contains(",")) { // add support later
					optionsBox.setSelectedItem(savedOptions);
				}
			}
			schemaPanel.setVisible(flashViewController.hasVisibleSchematicFile());
			schemaLabel.setIcon(null);
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					SwingUtilities.updateComponentTreeUI(frame);
				}
			});
			
			schemaMap.clear();
			
			for (FlashFirmwareSet ffs: flashViewController.getFlashInfo().getFlashFirmwareSets()) {
				for (FlashFirmware ff:ffs.getFirmwares()) {
					if (ff.getSchematicFile() == null) {
						continue;
					}
					if (ff.getSchematicFile().isEmpty()) {
						continue;
					}
					try {
						InputStream is = flashSwingUIIntegration.loadSchemaImage(ff);
						if (is == null) {
							logger.warning("Couln't load image resource: "+ff.getSchematicFile());
							continue;
						}
						Image image = ImageIO.read(is);
						schemaMap.put(ff.getName(), new ImageIcon(image)); // TODO: fix keying per set
					} catch (IOException e) {
						logger.warning("Could not load schema image: "+e.getMessage());
					}
				}
			}
		} finally {
			allowSelectionEvent = true;
		}
	}
	
	private void updateSelection() {
		FlashFirmware firmware = flashViewController.getSelectedFirmware();
		
		boolean firmwareSelected = firmware!=null; 
		flashButton.setEnabled(firmwareSelected);
		exportButton.setEnabled(firmwareSelected);
		portsComboBox.setEnabled(firmwareSelected);
		progComboBox.setEnabled(firmwareSelected);
		progVerboseBox.setEnabled(firmwareSelected);
		progVerifyBox.setEnabled(firmwareSelected);
		
		burnName.setText(firmware!=null?firmware.getName():"");
		
		if (firmware!=null && firmware.getProgrammer()!=null) {
			FlashFirmwareSetProgrammer prog = firmware.getProgrammer();
			if (prog.getFlashVerify()!=null) {
				progVerifyBox.setSelected(prog.getFlashVerify());
			}
			if ("stk500v2".equals(prog.getProtocol())) { // TODO: rm hardcoded here
				progComboBox.setSelectedIndex(1);
			} else {
				progComboBox.setSelectedIndex(0);
			}
			if (prog.getMcuSignature()==null) {
				burnDeviceId.setText("null");
			} else {
				burnDeviceId.setText(String.format("0x%X",prog.getMcuSignature()));
			}
			
		} else {
			burnDeviceId.setText("");
		}
		
		if (firmware==null) {
			schemaLabel.setIcon(null);
		} else if (firmware.getSchematicFile() == null) {
			schemaLabel.setIcon(null);
		} else {
			schemaLabel.setIcon(schemaMap.get(firmware.getName()));
		}
	}
	
	private JPanel createPanelTop() {
		JPanel filterHeaderPanel = flashSwingUIIntegration.createTitledBorderPanel("Filter options");
		
		JPanel tableOption = new JPanel();
		tableOption.setLayout(new SpringLayout());
		JLabel mcuText = new JLabel("Type:");
		tableOption.add(mcuText);
		firmwareSetBox = new JComboBox<String>();
		firmwareSetBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (!allowSelectionEvent) {
					return;
				}
				if (firmwareSetBox.getSelectedIndex()==0) {
					flashViewController.setFilterFirmwareSet(null);
				} else {
					flashViewController.setFilterFirmwareSet(firmwareSetBox.getModel().getElementAt(firmwareSetBox.getSelectedIndex()));
				}
			}
		});
		tableOption.add(firmwareSetBox);
		
		JLabel optionText = new JLabel("Options:");
		tableOption.add(optionText);
		optionsBox = new JComboBox<String>();
		optionsBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (!allowSelectionEvent) {
					return;
				}
				if (optionsBox.getSelectedIndex()==0) {
					flashViewController.clearFilterOptions();
				} else {
					String[] op = optionsBox.getModel().getElementAt(optionsBox.getSelectedIndex()).split("="); // TODO: redo
					flashViewController.putFilterOption(op[0], op[1]);
				}
			}
		});
		tableOption.add(optionsBox);
		
		SpringLayoutGrid.makeCompactGrid(tableOption,4,1);
		filterHeaderPanel.add(tableOption);
		
		
		JPanel filterFlagsPanel = flashSwingUIIntegration.createTitledBorderPanel("Filter Flags");
		filterFlags = new JList<>();
		filterFlags.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		filterFlags.addListSelectionListener(new ListSelectionListener() {
			
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if (!allowSelectionEvent) {
					return;
				}
				if (e.getValueIsAdjusting()) {
					return;
				}
				flashViewController.clearFilterFlags();
				for (int idx: filterFlags.getSelectedIndices()) {
					String flag = flashViewController.getTotalFlashFlags().get(idx); // TODO: write correct code
					flashViewController.putFilterFlag(flag, Boolean.TRUE);
				}
			}
		});
		
		final JScrollPane listScroller = new JScrollPane(filterFlags);
		filterFlagsPanel.add(listScroller);
		filterFlagsPanel.setMinimumSize(new Dimension(120, 100));
		
		JPanel optionWrapPanel = new JPanel();
		optionWrapPanel.setLayout(new SpringLayout());
		optionWrapPanel.add(filterHeaderPanel);
		optionWrapPanel.add(filterFlagsPanel);
		SpringLayoutGrid.makeCompactGrid(optionWrapPanel,2,1);
		
		return optionWrapPanel;
	}
	
	private JComponent createPanelCenter() {
		tableModel = new FlashFirmwareTableModel(flashViewController);
		table = new JTable(tableModel);
		table.setShowGrid(false);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if (e.getValueIsAdjusting()) {
					return;
				}
				int selectedRow = table.getSelectedRow();
				if (selectedRow==-1) {
					flashViewController.setSelectedFirmware(null);
					return;
				}
				FlashFirmware firmware = (FlashFirmware)tableModel.getValueAt(selectedRow, 0);
				flashViewController.setSelectedFirmware(firmware);
			}
		});
		
		FlashFirmwareTableRenderer firmwareRenderer = new FlashFirmwareTableRenderer(flashViewController);
		table.setRowHeight(firmwareRenderer.getPreferredSize().height);
		table.setDefaultRenderer(FlashFirmware.class, firmwareRenderer);
		
		ToolTipManager.sharedInstance().unregisterComponent(table);
		ToolTipManager.sharedInstance().unregisterComponent(table.getTableHeader());
		
		// disable table header stuff
		table.setTableHeader(null);
		
		JScrollPane scroll = new JScrollPane(table);
		
		schemaPanel = new JPanel(new BorderLayout());
		schemaLabel = new JLabel();
		schemaPanel.add(schemaLabel,BorderLayout.CENTER);
		JLabel schemaText = new JLabel("Schematic;");
		schemaText.setPreferredSize(new Dimension(200, 20));
		schemaPanel.add(schemaText,BorderLayout.NORTH);
		
		JPanel centerPanel = new JPanel();
		centerPanel.setLayout(new BorderLayout());
		centerPanel.add(scroll,BorderLayout.CENTER);
		centerPanel.add(schemaPanel,BorderLayout.EAST);
		
		return centerPanel;
	}
	
	private JPanel createPanelBottom() {
		JPanel burnPanel = flashSwingUIIntegration.createTitledBorderPanel("Burn");
		burnPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		
		JPanel burnOptionPanel = new JPanel();
		burnOptionPanel.setLayout(new SpringLayout());
		burnOptionPanel.add(new JLabel("Name:"));
		burnName = new JLabel();
		burnOptionPanel.add(burnName);
		burnOptionPanel.add(new JLabel("DeviceId:"));
		burnDeviceId = new JLabel();
		burnOptionPanel.add(burnDeviceId);
		burnOptionPanel.add(new JLabel("Port:"));
		SerialPortsComboBoxModel portModel = new SerialPortsComboBoxModel();
		portsComboBox = new JComboBox<String>(portModel);
		portsComboBox.addPopupMenuListener(portModel.getPopupMenuListener());
		burnOptionPanel.add(portsComboBox);
		burnOptionPanel.add(new JLabel("Programer:"));
		
		String nativeFlashCmd = flashViewController.getFlashControllerConfig().getNativeFlashCmd();
		String nativeFlashConfig = flashViewController.getFlashControllerConfig().getNativeFlashConfig();
		if (nativeFlashCmd!=null && nativeFlashCmd.isEmpty()==false && nativeFlashConfig!=null && nativeFlashConfig.isEmpty()==false) {
			progComboBox = new JComboBox<String>(new String[] {"arduino","stk500v2","native-arduino","native-stk500v1","native-stk500v2"});
		} else {
			progComboBox = new JComboBox<String>(new String[] {"arduino","stk500v2"});
		}
		burnOptionPanel.add(progComboBox);
		burnOptionPanel.add(new JLabel("logVerbose:"));
		progVerboseBox = new JCheckBox();
		burnOptionPanel.add(progVerboseBox);
		burnOptionPanel.add(new JLabel("flashVerify:"));
		progVerifyBox = new JCheckBox();
		burnOptionPanel.add(progVerifyBox);
		SpringLayoutGrid.makeCompactGrid(burnOptionPanel,6,2);
		burnPanel.add(burnOptionPanel);
		
		JPanel burnProgressPanel = new JPanel();
		burnProgressPanel.setLayout(new GridLayout(1,1));
		burnProgressPanel.setBorder(BorderFactory.createEmptyBorder(6,0,12,0));
		flashProgressBar = new JProgressBar();
		flashProgressBar.setStringPainted(true);
		burnProgressPanel.add(flashProgressBar);
		
		JPanel logPanel = flashSwingUIIntegration.createTitledBorderPanel("Burn Log");
		flashLogTimeFormat = new SimpleDateFormat("HH:mm:ss");
		flashLog = new JTextArea(10,50);
		flashLog.setMargin(new Insets(2, 2, 2, 2));
		flashLog.setAutoscrolls(true);
		flashLog.setEditable(false);
		JScrollPane consoleScrollPane = new JScrollPane(flashLog);
		consoleScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		consoleScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		consoleScrollPane.getViewport().setOpaque(false);
		logPanel.add(consoleScrollPane);
		appendFlashLog("Ready to flash.");
		
		JPanel burnActionPanel = new JPanel();
		burnActionPanel.setBorder(BorderFactory.createEmptyBorder());
		burnActionPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		exportButton = new JButton("Export");
		exportButton.setEnabled(false);
		exportButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (burnName.getText().isEmpty()) {
					return;
				}
				JFileChooser fileSelect = new JFileChooser();
				fileSelect.setSelectedFile(new File(flashSwingUIIntegration.getExportFilePrefix()+burnName.getText()+".hex"));
				int returnVal = fileSelect.showSaveDialog((JButton)e.getSource());
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					try {
						exportToFile(fileSelect.getSelectedFile());
					} catch (IOException e1) {
						JOptionPane.showMessageDialog(exportButton, "Could not save file: "+e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		});
		burnActionPanel.add(exportButton);
		flashButton = new JButton("Flash");
		flashButton.setEnabled(false);
		flashButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				startFlash();
			}
		});
		burnActionPanel.add(flashButton);
		closeButton = new JButton("Close");
		closeButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				flashSwingUIIntegration.doWindowClose(flashViewController, frame);
			}
		});
		burnActionPanel.add(closeButton);
		
		JPanel burnWrapPanel = new JPanel();
		burnWrapPanel.setLayout(new BorderLayout());
		burnWrapPanel.add(burnProgressPanel,BorderLayout.PAGE_START);
		burnWrapPanel.add(burnPanel,BorderLayout.CENTER);
		burnWrapPanel.add(logPanel,BorderLayout.LINE_END);
		burnWrapPanel.add(burnActionPanel,BorderLayout.PAGE_END);
		return burnWrapPanel;
	}
	
	public void exportToFile(File exportFile) throws IOException {
		InputStream is = null;
		OutputStream os = null;
		try {
			is = this.flashSwingUIIntegration.loadHexFile(flashViewController.getSelectedFirmware());
			os = new FileOutputStream(exportFile);
			byte[] buf = new byte[4096];
			int cnt = is.read(buf);
			while (cnt > 0) {
				os.write(buf, 0, cnt);
				cnt = is.read(buf);
			}
		} finally {
			if (os!=null) {
				os.close();
			}
			if (is!=null) {
				is.close();
			}
		}
	}
	
	private void startFlash() {
		if (flashViewController.getSelectedFirmware() == null) {
			return;
		}
		logger.info("Start chip flash of: "+flashViewController.getSelectedFirmware().getName());
		byte[] flashData = null;
		try {
			flashData = new FlashHexReader().loadHex(flashSwingUIIntegration.loadHexFile(flashViewController.getSelectedFirmware()));
		} catch (Exception hexException) {
			logger.log(Level.WARNING,hexException.getMessage(),hexException);
			JOptionPane.showMessageDialog(frame,"There has been an error in loading or parsing the flash hex data correctly.\nMessage: "+hexException.getMessage()+" \nFile: "+flashViewController.getSelectedFirmware().getFirmwareFile(),"Hex data error",JOptionPane.WARNING_MESSAGE);
			return;
		}
		frame.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		
		portsComboBox.setEnabled(false);
		progComboBox.setEnabled(false);
		progVerboseBox.setEnabled(false);
		progVerifyBox.setEnabled(false);
		flashButton.setEnabled(false);
		closeButton.setEnabled(false);
		table.setEnabled(false);
		if (flashLog.getText().length()>32) {
			flashLog.setText(""); // clear log for second flash 
		}
		FlashControllerConfig flashConfig = flashViewController.getFlashControllerConfig();
		flashConfig.setPort(portsComboBox.getSelectedItem().toString());
		flashConfig.setPortProtocol(progComboBox.getSelectedItem().toString());
		flashConfig.setLogDebug(progVerboseBox.isSelected());
		flashConfig.setFlashVerify(progVerifyBox.isSelected());
		flashConfig.setFlashData(flashData);
		String deviceId = burnDeviceId.getText();
		if (deviceId!=null && deviceId.isEmpty()==false && deviceId.startsWith("0x") && deviceId.length()>2) {
			flashConfig.setDeviceSignature(Integer.parseInt(burnDeviceId.getText().substring(2),16));
		}
		FlashThread flasher = new FlashThread();
		flasher.start();
	}
	
	private FlashEventListener flashListener = new FlashEventListener() {
		
		@Override
		public void flashLogMessage(String message) {
			appendFlashLog(message);
		}
		
		@Override
		public void progressUpdate(int progress) {
			flashProgressBar.getModel().setValue(progress);
		}
	};
	
	private void appendFlashLog(String data) {	
		flashLog.append(flashLogTimeFormat.format(new Date()));
		flashLog.append(" # ");
		flashLog.append(data);
		flashLog.append("\n");
		flashLog.repaint();
		flashLog.setCaretPosition(flashLog.getText().length()); // auto scroll to end
	}
	
	class FlashThread extends Thread {
		
		@Override
		public void run() {
			FlashProgramController flashProgramController = null;
			try {
				logger.fine("Start flash thread.");
				flashProgramController = FlashManager.createFlashController(flashViewController.getFlashControllerConfig());
				flashProgramController.addFlashEventListener(flashListener);
				flashProgramController.flash(flashViewController.getFlashControllerConfig());
			} catch (Exception runException) {
				logger.log(Level.WARNING,runException.getMessage(),runException);
			} finally {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						flashProgressBar.getModel().setValue(0);
						portsComboBox.setEnabled(true);
						progComboBox.setEnabled(true);
						progVerboseBox.setEnabled(true);
						progVerifyBox.setEnabled(true);
						flashButton.setEnabled(true);
						closeButton.setEnabled(true);
						table.setEnabled(true);
						FlashSwingUI.this.frame.setCursor(Cursor.getDefaultCursor());
					}
				});
				if (flashProgramController != null) {
					flashProgramController.removeFlashEventListener(flashListener);
				}
				logger.fine("Stopped flash thread.");
			}
		}
	}
}
