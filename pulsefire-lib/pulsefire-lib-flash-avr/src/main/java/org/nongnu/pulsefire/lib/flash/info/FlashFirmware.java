/*
 * Copyright (c) 2011, Willem Cazander
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this list of conditions and the
 *   following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 *   the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.nongnu.pulsefire.lib.flash.info;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 * FlashFirmware holds all info of a firmware we can flash onto an device.
 * 
 * @author Willem Cazander
 */
public class FlashFirmware {
	
	private String name;
	private String description;
	private Boolean visible;
	private String firmwareFile;
	private String schematicFile;
	private FlashFirmwareSetProgrammer programmer;
	private final List<String> firmwareFlags;
	private final List<FlashFirmwareOption> firmwareOptions;
	
	public FlashFirmware() {
		firmwareFlags = new ArrayList<String>(20);
		firmwareOptions = new ArrayList<FlashFirmwareOption>(20);
	}
	
	public FlashFirmware(String name,String firmwareFile,String schematicFile) {
		this();
		setName(name);
		setFirmwareFile(firmwareFile);
		setSchematicFile(schematicFile);
	}
	
	/**
	 * @return the name
	 */
	@XmlAttribute(name=AbstractXMLMarshallerAttribute.NAME,required=AbstractXMLMarshallerMeta.REQUIRED)
	public String getName() {
		return name;
	}
	
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * @return the description
	 */
	@XmlAttribute(name=AbstractXMLMarshallerAttribute.DESCRIPTION)
	public String getDescription() {
		return description;
	}
	
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	/**
	 * @return the visible
	 */
	@XmlAttribute(name=AbstractXMLMarshallerAttribute.VISIBLE)
	public Boolean getVisible() {
		return visible;
	}
	
	/**
	 * @param visible the visible to set
	 */
	public void setVisible(Boolean visible) {
		this.visible = visible;
	}
	
	/**
	 * @return the firmwareFile
	 */
	@XmlAttribute(name=AbstractXMLMarshallerAttribute.FILE,required=AbstractXMLMarshallerMeta.REQUIRED)
	public String getFirmwareFile() {
		return firmwareFile;
	}
	
	/**
	 * @param firmwareFile the firmwareFile to set
	 */
	public void setFirmwareFile(String firmwareFile) {
		this.firmwareFile = firmwareFile;
	}
	
	/**
	 * @return the schematicFile
	 */
	@XmlAttribute(name=AbstractXMLMarshallerAttribute.SCHEMATIC)
	public String getSchematicFile() {
		return schematicFile;
	}
	
	/**
	 * @param schematicFile the schematicFile to set
	 */
	public void setSchematicFile(String schematicFile) {
		this.schematicFile = schematicFile;
	}
	
	/**
	 * @return the programmer
	 */
	@XmlElement(name=AbstractXMLMarshallerElement.FIRMWARE_PROGRAMMER)
	public FlashFirmwareSetProgrammer getProgrammer() {
		return programmer;
	}
	
	/**
	 * @param programmer the programmer to set
	 */
	public void setProgrammer(FlashFirmwareSetProgrammer programmer) {
		this.programmer = programmer;
	}
	
	/**
	 * @return the firmwareFlags
	 */
	@XmlElement(name=AbstractXMLMarshallerElement.FIRMWARE_FLAG)
	public List<String> getFirmwareFlags() {
		return firmwareFlags;
	}
	
	/**
	 * @param firmwareFlag the firmwareFlag to add
	 */
	public void addFirmwareFlag(String firmwareFlag) {
		firmwareFlags.add(firmwareFlag);
	}
	
	/**
	 * @return the firmwareOptions
	 */
	@XmlElement(name=AbstractXMLMarshallerElement.FIRMWARE_OPTION)
	public List<FlashFirmwareOption> getFirmwareOptions() {
		return firmwareOptions;
	}
	
	/**
	 * @param firmwareOption the firmwareOption to add
	 */
	public void addFirmwareOption(FlashFirmwareOption firmwareOption) {
		firmwareOptions.add(firmwareOption);
	}
}
