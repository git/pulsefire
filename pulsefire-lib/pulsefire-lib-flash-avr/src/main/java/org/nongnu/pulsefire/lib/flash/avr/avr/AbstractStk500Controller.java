/*
 * Copyright (c) 2011, Willem Cazander
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this list of conditions and the
 *   following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 *   the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.nongnu.pulsefire.lib.flash.avr.avr;

import java.util.logging.Logger;

import jssc.SerialPort;
import jssc.SerialPortException;
import jssc.SerialPortTimeoutException;

import org.nongnu.pulsefire.lib.flash.avr.AbstractFlashProgramController;
import org.nongnu.pulsefire.lib.flash.avr.FlashControllerConfig;
import org.nongnu.pulsefire.lib.flash.avr.FlashException;

/**
 * AbstractStk500Controller is the base for stk500 based flash devices.
 * 
 * @author Willem Cazander
 */
abstract public class AbstractStk500Controller extends AbstractFlashProgramController {

	protected Logger logger = null;
	protected SerialPort serialPort = null;
	protected int timeout = 1500;
	
	public AbstractStk500Controller() {
		logger = Logger.getLogger(AbstractStk500Controller.class.getName());
	}
	
	public FlashMessage sendFlashMessage(FlashMessage message) throws SerialPortException,SerialPortTimeoutException {
		if (message==null) {
			throw new NullPointerException("Can't send null message");
		}
		if (message.getRequest().isEmpty()) {
			throw new IllegalArgumentException("Can't send empty message");
		}
		StringBuilder buf = new StringBuilder(30);
		for (Integer data:message.getRequest()) {
			serialPort.writeInt(data);
			String hex = Integer.toHexString(data);
			if (hex.length()==1) {
				hex = "0"+hex;
			}
			if (hex.startsWith("ffffff")) {
				hex = hex.substring(6);
			}
			buf.append(hex);
		}
		// maybe: check again with jssc and remove this left over of rxtx
		try { // This sleep is needed for windows flashing to work correctly.
			Thread.sleep(15); // (note: it worked when logDebug was enabled)
		} catch (InterruptedException e) {
		}
		return readFlashMessage(message,buf);
	}
	abstract protected FlashMessage readFlashMessage(FlashMessage message,StringBuilder buf) throws SerialPortException,SerialPortTimeoutException;
	abstract protected void prepareMessagePrefix(FlashMessage msg,FlashCommandToken command);
	abstract protected void prepareMessagePostfix(FlashMessage msg,FlashCommandToken command);
	
	public FlashMessage doFlashCommand(FlashCommandToken command,Integer...param) throws SerialPortException,SerialPortTimeoutException {
		FlashMessage msg = new FlashMessage();
		prepareMessagePrefix(msg,command);
		for (Integer data:param) {
			msg.getRequest().add(data);
		}
		prepareMessagePostfix(msg,command);
		return sendFlashMessage(msg);
	}
	
	protected void rebootDevice() throws SerialPortException {
		logMessage("Reboot device.");
		
		serialPort.setRTS(false);
		serialPort.setDTR(false);
		try {
			Thread.sleep(150);
		} catch (InterruptedException e) {
			logger.fine("woken");
		}
		serialPort.setRTS(true);
		serialPort.setDTR(true);
		try {
			Thread.sleep(150);
		} catch (InterruptedException e) {
			logger.fine("woken");
		}
		serialPort.purgePort(SerialPort.PURGE_RXCLEAR + SerialPort.PURGE_TXCLEAR);
	}
	
	protected void disconnectPort() throws FlashException  {
		if (serialPort==null) {
			return;
		}
		try {
			serialPort.closePort();
		} catch (Exception e) {
			logger.warning("Could not close: "+e.getMessage());
		}
		serialPort = null;
		logMessage("Disconnected from port.");
	}
	
	protected void connectPort(FlashControllerConfig flashControllerConfig) throws FlashException {
		try {
			serialPort = new SerialPort(flashControllerConfig.getPort());
			serialPort.openPort();
			serialPort.setParams(SerialPort.BAUDRATE_115200, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
			serialPort.setFlowControlMode(SerialPort.FLOWCONTROL_NONE); 
			logMessage("Connected to port: "+serialPort.getPortName());		
		} catch (SerialPortException e1) {
			throw new FlashException("Error while connect to port: "+e1.getMessage(),e1);
		}
	}
	
	protected void logConfig(FlashControllerConfig flashControllerConfig) {
		logMessage("Flash port: "+flashControllerConfig.getPort());
		logMessage("Flash data size: "+flashControllerConfig.getFlashData().length);
		logMessage("Flash protocol: "+flashControllerConfig.getPortProtocol());
		logMessage("Flash verify: "+flashControllerConfig.isFlashVerify());
	}
	
	abstract protected void flashSafe(FlashControllerConfig flashControllerConfig) throws FlashException,SerialPortException,SerialPortTimeoutException;
	
	protected void flashSafeDisconnect(FlashControllerConfig flashControllerConfig) {
		// hook point for clean up.
	}
	
	public void flashTimedRun(FlashControllerConfig flashControllerConfig) throws FlashException {
		try {
			flashSafe(flashControllerConfig);
		} catch (FlashException flashException) {
			logMessage("Error: "+flashException.getMessage());
			throw flashException;
		} catch (Exception codeException) {
			logMessage("CError: "+codeException.getMessage());
			throw new FlashException(codeException.getMessage(),codeException);
		} finally {
			try {
				flashSafeDisconnect(flashControllerConfig);
			} finally {
				disconnectPort(); // always disconnect
			}
		}
	}
}
