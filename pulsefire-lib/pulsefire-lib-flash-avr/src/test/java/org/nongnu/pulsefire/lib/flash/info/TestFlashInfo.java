package org.nongnu.pulsefire.lib.flash.info;

import org.junit.Test;
import org.nongnu.pulsefire.lib.flash.info.FlashFirmware;
import org.nongnu.pulsefire.lib.flash.info.FlashFirmwareSet;
import org.nongnu.pulsefire.lib.flash.info.FlashFirmwareSetProgrammer;
import org.nongnu.pulsefire.lib.flash.info.FlashInfo;
import org.nongnu.pulsefire.lib.flash.info.XMLMarshallerFlashInfo;

public class TestFlashInfo {
	
	@Test
	public void testFlashOption() throws Exception {
		
		XMLMarshallerFlashInfo xmlFlashOption = new XMLMarshallerFlashInfo();
		
		FlashInfo fo = new FlashInfo();
		fo.setDefaultFirmwareSet("atmega168p");
		
		FlashFirmwareSetProgrammer fosIsp = new FlashFirmwareSetProgrammer();
		fosIsp.setMcuSignature(0x1e940b);
		fosIsp.setMcuType("mega16p");
		fosIsp.setProtocol("arduino");
		fosIsp.setFlashVerify(true);
		
		FlashFirmwareSet fos = new FlashFirmwareSet("atmega168p");
		fos.setProgrammer(fosIsp);
		fo.addFlashFirmwareSet(fos);
		
		FlashFirmwareSet fos16 = new FlashFirmwareSet("atmega168p-16mhz");
		fos16.addFirmwareOption(new FlashFirmwareOption("F_CPU","16000000"));
		fos16.setVisible(false);
		fos.addFlashFirmwareSet(fos16);
		
		FlashFirmware firm1 = new FlashFirmware("simple","atmega168p/test/simple.hex","atmega168p/schema/simple.png");
		firm1.addFirmwareFlag("LCD");
		firm1.addFirmwareFlag("PWM");
		fos16.addFirmware(firm1);
		
		FlashFirmware firm2 = new FlashFirmware("big","atmega168p/test/big.hex","atmega168p/schema/big.png");
		firm2.addFirmwareFlag("LCD");
		fos16.addFirmware(firm2);
		
		FlashInfoText fit = new FlashInfoText();
		fit.setTitle("testTitle");
		fit.setDescription("this is unit test");
		fit.setResourceBundleFile("some/where/in/zip");
		fit.addFirmwareFlag(new FlashInfoTextValue("LCD", "Liques crytal dipay supprot"));
		fit.addFirmwareFlag(new FlashInfoTextValue("PWM", "pulse with modulation supprot"));
		fit.addFirmwareOption(new FlashInfoTextValue("F_CPU", "Speed"));
		
		fo.setInfoText(fit);
		
		xmlFlashOption.marshal(fo, System.out);
	}
}
